variable "bucket_name" {
  description = "The name of the S3 bucket"
  type        = string
}

variable "ses_rule_set_name" {
  description = "The name of the SES rule set"
  type        = string
  default     = "default-rule-set"
}

variable "email_recipients" {
  description = "A list of email addresses to receive mail for"
  type        = list(string)
}

variable "object_key_prefix" {
  description = "The key prefix to use when saving emails to S3"
  type        = string
  default     = "emails/"
}

variable "topic_name" {
  description = "name of the SNS topic"
  type        = string
}

variable "endpoint" {
  description = "The HTTPS endpoint for SNS notifications"
  type        = string
}

variable "receipt_rule_name" {
  type = string
}

## SQS variables
variable "queue_name" {
  description = "Name of the queue"
  type        = string
}
variable "message_retention_seconds" {
  description = "he number of seconds Amazon SQS retains a message. Integer representing seconds, from 60 (1 minute) to 1209600 (14 days). The default for this attribute is 345600 (4 days)."
  type        = number
  default     = 345600
}
