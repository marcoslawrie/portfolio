// terraform/main.tf
terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }

}

module "s3_bucket" {
  source      = "../modules/s3"
  bucket_name = var.bucket_name
}

module "ses" {
  source            = "../modules/ses"
  rule_set_name     = var.ses_rule_set_name
  email_recipients  = var.email_recipients
  bucket_name       = module.s3_bucket.bucket_name
  object_key_prefix = var.object_key_prefix
  topic_arn         = module.sns_topic.topic_arn
  receipt_rule_name = var.receipt_rule_name
}

module "sns_topic" {
  source                = "../modules/sns"
  topic_name            = var.topic_name
  receipt_rule_set_name = var.ses_rule_set_name
  receipt_rule_name     = var.receipt_rule_name

}

module "sns_subscriptions" {
  source    = "../modules/sns-subscription"
  topic_arn = module.sns_topic.topic_arn
  endpoint  = var.endpoint
  sqs_arn   = module.sqs.sqs_arn
}

#module "s3-notification" {
#  source    = "../modules/s3-notification"
#  bucket_id = module.s3_bucket.bucket_id
#  topic_arn = module.sns_topic.topic_arn
#
#}

module "sqs" {
  source                    = "../modules/sqs"
  queue_name                = var.queue_name
  message_retention_seconds = var.message_retention_seconds
}


terraform {
  backend "http" {}
}