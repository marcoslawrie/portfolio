output "s3_bucket_name" {
  description = "The name of the S3 bucket"
  value       = module.s3_bucket.bucket_name
}

output "ses_rule_set_name" {
  description = "The name of the SES rule set"
  value       = module.ses.rule_set_name
}
