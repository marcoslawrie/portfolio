resource "aws_sns_topic" "email_notifications" {
  name = var.topic_name
  policy = data.aws_iam_policy_document.topic.json
#  fifo_topic                  = true

}

data "aws_iam_policy_document" "topic" {
  statement { 
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["ses.amazonaws.com"]
    }

    actions   = ["SNS:Publish"]
    resources = ["arn:aws:sns:*:*:${var.topic_name}"]

    condition {
      test     = "ArnLike"
      variable = "aws:SourceArn"
      values   = ["arn:aws:ses:*:*:receipt-rule-set/${var.receipt_rule_set_name}:receipt-rule/${var.receipt_rule_name}"]
    }
  }

#  statement { 
#    effect = "Allow"
#
#    principals {
#      type        = "Service"
#      identifiers = ["sqs.amazonaws.com"]
#    }
#
#    actions   = ["SNS:Subscribe"]
#    resources = ["arn:aws:sns:*:*:${var.topic_name}"]
#
#    condition {
#      test     = "ArnLike"
#      variable = "aws:SourceArn"
#      values   = ["arn:aws:sqs:*"]
#    }
#  }

}