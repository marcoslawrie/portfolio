variable "topic_name"{
    description = "Name of the SNS topic"
    type = string
}

variable "receipt_rule_set_name" {
  type = string
}

variable "receipt_rule_name" {
  type = string
}