variable "queue_name" {
    description = "Name of the queue"
    type = string
}
variable "max_message_size" {
    description = "The limit of how many bytes a message can contain before Amazon SQS rejects it. An integer from 1024 bytes (1 KiB) up to 262144 bytes (256 KiB). The default value for this attribute is 262144 (256 KiB)"
    type = number
    default = 262144 
}
variable "message_retention_seconds" {
    description = "he number of seconds Amazon SQS retains a message. Integer representing seconds, from 60 (1 minute) to 1209600 (14 days). The default for this attribute is 345600 (4 days)."
    type = number
    default = 345600
}
variable "sqs_managed_sse_enabled" {
    description = "Boolean to enable server-side encryption (SSE) of message content with SQS-owned encryption keys. Default true"
    type = bool
    default = true
}

variable "fifo_queue" {
    description = "Boolean designating a FIFO queue. If not set, it defaults to false"
    type = bool
    default = false
}

variable "visibility_timeout_seconds" {
    description = "Time in seconds before a message becomes visible again to other consumers if system doesn't call the DeleteMessage. Default value of this attribute is 30"
    type = number
    default = 30
}

variable "redrive_policy" {
    description = ""
    type   = string
    default = ""
}