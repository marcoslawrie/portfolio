resource "aws_sqs_queue" "terraform_queue" {
  name                      = var.queue_name
  max_message_size          = var.max_message_size
  message_retention_seconds = var.message_retention_seconds
  sqs_managed_sse_enabled   = var.sqs_managed_sse_enabled
  fifo_queue                = var.fifo_queue
  visibility_timeout_seconds = var.visibility_timeout_seconds
  redrive_policy            = var.redrive_policy
}   