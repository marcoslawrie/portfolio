variable "bucket_id" {
  description = "The id of the S3 email bucket"
  type        = string
}

variable "topic_arn"{
    description = "ARN of the topic"
    type = string
}