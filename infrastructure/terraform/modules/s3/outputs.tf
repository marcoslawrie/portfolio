output "bucket_name" {
  description = "The name of the S3 bucket"
  value       = aws_s3_bucket.email_bucket.bucket
}

output "bucket_arn" {
  description = "The ARN of the S3 bucket"
  value       = aws_s3_bucket.email_bucket.arn
}

output "bucket_id" {
    description = "The id of the S3 bucket"
    value = aws_s3_bucket.email_bucket.id
}