variable "topic_arn" {
  description = "The ARN of the SNS topic"
  type        = string
}

variable "endpoint" {
  description = "The HTTPS endpoint for SNS notifications"
  type        = string
}

variable "sqs_arn" {
  description = "ARN of the sqs queue"
  type        = string
}
