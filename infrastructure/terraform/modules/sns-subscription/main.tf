#resource "aws_sns_topic_subscription" "https_subscription" {
#  topic_arn = var.topic_arn
#  protocol  = "https"
#  endpoint  = var.endpoint
#}

resource "aws_sns_topic_subscription" "https_subscription" {
  topic_arn = var.topic_arn
  protocol  = "sqs"
  endpoint  = var.sqs_arn
  endpoint_auto_confirms = true
}