output "rule_set_name" {
  description = "The name of the SES rule set"
  value       = aws_ses_receipt_rule_set.default.rule_set_name
}