variable "rule_set_name" {
  description = "The name of the SES rule set"
  type        = string
}

variable "email_recipients" {
  description = "A list of email addresses to receive mail for"
  type        = list(string)
}

variable "bucket_name" {
  description = "The name of the S3 bucket to save emails to"
  type        = string
}

variable "object_key_prefix" {
  description = "The key prefix to use when saving emails to S3"
  type        = string
  default     = "emails/"
}

variable "topic_arn" {
  description = "arn of the sns topic"
  type        = string
}

variable "receipt_rule_name"{
  type = string
}