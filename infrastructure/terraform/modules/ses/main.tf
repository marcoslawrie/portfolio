// terraform-modules/ses/main.tf

resource "aws_ses_receipt_rule_set" "default" {
  rule_set_name = var.rule_set_name
}

resource "aws_ses_receipt_rule" "s3_rule" {
  name          = var.receipt_rule_name
  rule_set_name = aws_ses_receipt_rule_set.default.rule_set_name
  enabled       = true
  scan_enabled  = true

  recipients = var.email_recipients

  s3_action {
    bucket_name       = var.bucket_name
    object_key_prefix = var.object_key_prefix
    topic_arn         = var.topic_arn
    position          = 1
  }

  tls_policy = "Optional"
}

